<?php

namespace Altra\Amqp\Tests;

use Altra\Amqp\AmqpConnectorServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

abstract class TestCase extends Orchestra
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app)
    {
        return [
            AmqpConnectorServiceProvider::class,
        ];
    }

    // protected function setProtectedProperty($class, $mock, $propertyName, $value)
  // {
  //     $reflectionClass = new \ReflectionClass($class);
  //     $channelProperty = $reflectionClass->getProperty($propertyName);
  //     $channelProperty->setAccessible(true);
  //     $channelProperty->setValue($mock, $value);
  //     $channelProperty->setAccessible(false);
  // }
}
