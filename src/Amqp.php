<?php

namespace Altra\Amqp;

use Altra\Amqp\Contracts\AmqpContract;
use Closure;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class Amqp implements AmqpContract
{
    public function __construct()
    {
    }

    /**
     * @param  string  $routing
     * @param  mixed  $message
     * @param  array  $properties
     * @return bool|null
     *
     * @throws Exception\Configuration
     * @throws \Exception
     */
    public function publish(mixed $message, array $exchange, string $routing, array $queues = null): bool
    {
        $publisher = App::make('amqp_publisher');

        return $publisher->publish($message, $exchange, $routing, $queues);
    }

    public function publishBatch(Collection $collection, array $exchange, string $routing, array $queues = null): bool
    {
        $publisher = App::make('amqp_publisher');

        return $publisher->publishBatch($collection, $exchange, $routing, $queues);
    }

    public function consume(string $queue, Closure $callback, array $properties = []): void
    {
        $consumer = App::make('amqp_consumer');

        $consumer->consume($queue, $callback);
    }
}
