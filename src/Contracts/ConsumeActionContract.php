<?php

namespace Altra\Amqp\Contracts;

use PhpAmqpLib\Message\AMQPMessage;

interface ConsumeActionContract
{
    public function execute(AMQPMessage $message): void;
}
