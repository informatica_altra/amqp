<?php

namespace Altra\Amqp\Contracts;

use PhpAmqpLib\Message\AMQPMessage;

interface ConsumerContract
{
    public function consume(string $queue, callable $callback): bool;

    public function acknowledge(AMQPMessage $message): void;

    public function reject(AMQPMessage $message, bool $requeue): void;
}
