<?php

namespace Altra\Amqp\Contracts;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

interface AmqpConnectionContract
{
    public function getChannel(): AMQPChannel;

    public function getConnection(): AMQPStreamConnection;

    public function shutdown(AMQPChannel $channel, AMQPStreamConnection $connection): void;
}
