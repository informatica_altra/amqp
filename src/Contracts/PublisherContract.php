<?php

namespace Altra\Amqp\Contracts;

use Illuminate\Support\Collection;

interface PublisherContract
{
    public function publish(mixed $message, array $exchange, string $routing, array $queues, bool $mandatory): bool;

    public function publishBatch(Collection $collection, array $exchange, string $routing, array $queues = null, bool $mandatory = false): bool;
}
