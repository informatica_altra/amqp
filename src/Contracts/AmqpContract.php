<?php

namespace Altra\Amqp\Contracts;

use Closure;
use Illuminate\Support\Collection;

interface AmqpContract
{
    public function publish(mixed $message, array $exchange, string $routing, array $queues): bool;

    public function publishBatch(Collection $collection, array $exchange, string $routing, array $queues = null): bool;

    public function consume(string $queue, Closure $callback, array $properties): void;
}
