<?php

namespace Altra\Amqp;

use Altra\Amqp\Commands\AmqpActionGenerator;
use Altra\Amqp\Commands\AmqpConsumeCommand;
use Illuminate\Support\ServiceProvider;

class AmqpConnectorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/amqp.php' => config_path('amqp.php'),
        ]);

        $this->publishes([
            __DIR__.'/../stubs/migrations/create_failed_consumers_table.php.stub' => database_path('migrations/'.date('Y_m_d_His', time()).'_create_failed_consumers_table.php'),
        ], 'migrations');

        if ($this->app->runningInConsole()) {
            $this->commands([
                AmqpActionGenerator::class,
                AmqpConsumeCommand::class,
            ]);
        }

        $this->app->bind('amqp', 'Altra\Amqp\Amqp');
        $this->app->bind('amqp_connection', config('amqp.broker'));
        $this->app->bind('amqp_publisher', config('amqp.publisher'));
        $this->app->bind('amqp_consumer', config('amqp.consumer'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
