<?php

namespace Altra\Amqp\Connections;

use Altra\Amqp\AmqpConnection;

class RabbitMQConnection extends AmqpConnection
{
    public function __construct()
    {
        parent::__construct(
      config('amqp.connections.rabbitmq.host'),
      config('amqp.connections.rabbitmq.port'),
      config('amqp.connections.rabbitmq.username'),
      config('amqp.connections.rabbitmq.password'),
    );
    }
}
