<?php

namespace Altra\Amqp\Testing;

use PHPUnit\Framework\Assert as PHPUnit;

class AmqpFake
{
    protected $events;

    public function __construct()
    {
        $this->events = collect();
    }

    public function publish(mixed $message, array $exchange, string $routing, array $queues = null): bool
    {
        $event = compact('message', 'exchange', 'routing', 'queues');
        $this->events->push($event);

        return true;
    }

    public function assertPublishedOnExchange(string $exchange): void
    {
        $published = $this->events->where('exchange.exchange', $exchange)->isNotEmpty();
        PHPUnit::assertTrue($published);
    }

    public function assertPublishedTimes(int $times): void
    {
        $message = "Events should have been published $times times";
        PHPUnit::assertSame($times, $this->events->count(), $message);
    }
}
