<?php

namespace Altra\Amqp;

use Illuminate\Database\Eloquent\Model;

class FailedConsumer extends Model
{
    protected $table = 'failed_consumers';

    protected $guarded = false;

    public $timestamps = false;
}
