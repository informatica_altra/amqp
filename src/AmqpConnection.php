<?php

namespace Altra\Amqp;

use Altra\Amqp\Contracts\AmqpConnectionContract;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

abstract class AmqpConnection implements AmqpConnectionContract
{
    /**
     * @var AMQPStreamConnection
     */
    protected $connection;

    protected $channel;

    public function __construct($host, $port, $username, $password)
    {
        $this->connect($host, $port, $username, $password);
    }

    public function connect($host, $port, $username, $password)
    {
        $this->connection = new AMQPStreamConnection(
      $host,
      $port,
      $username,
      $password,
    );

        $this->channel = $this->connection->channel();
    }

    /**
     * @return AMQPChannel
     */
    public function getChannel(): AMQPChannel
    {
        return $this->channel;
    }

    /**
     * @return AMQPStreamConnection
     */
    public function getConnection(): AMQPStreamConnection
    {
        return $this->connection;
    }

    /**
     * @param  AMQPChannel  $channel
     * @param  AMQPStreamConnection  $connection
     *
     * @throws \Exception
     */
    public function shutdown(AMQPChannel $channel, AMQPStreamConnection $connection): void
    {
        $channel->close();
        $connection->close();
    }
}
