<?php

namespace Altra\Amqp\Consumers;

use Altra\Amqp\Connections\RabbitMQConnection;
use Altra\Amqp\Contracts\ConsumerContract;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitConsumer extends RabbitMQConnection implements ConsumerContract
{
    public function consume(string $queue, callable $callback): bool
    {
        $channel = $this->getChannel();
        $channel->basic_consume(
      $queue,
      '',
      false,
      false,
      false,
      false,
      $callback,
    );

        while (count($this->getChannel()->callbacks)) {
            $this->getChannel()->wait(null, false, 0);
        }
        $this->shutdown($channel, $this->getConnection());

        return true;
    }

    public function acknowledge(AMQPMessage $message): void
    {
        $message->getChannel()->basic_ack($message->getDeliveryTag());

        if ($message->body === 'quit') {
            $message->getChannel()->basic_cancel($message->getConsumerTag());
        }
    }

    public function reject(AMQPMessage $message, $requeue = false): void
    {
        $message->getChannel()->basic_reject($message->getDeliveryTag(), $requeue);
    }
}
