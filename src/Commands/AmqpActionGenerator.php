<?php

namespace Altra\Amqp\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class AmqpActionGenerator extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'amqp:action {--folder}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new action for consuming message';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Action';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/../../stubs/action.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        if ($this->option('folder')) {
            return $rootNamespace.'\Actions\AmqpConsumers\\'.$this->option('folder');
        }

        return $rootNamespace.'\Actions\AmqpConsumers';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['folder', 'f', InputOption::VALUE_OPTIONAL, 'The name of the folder'],
        ];
    }
}
