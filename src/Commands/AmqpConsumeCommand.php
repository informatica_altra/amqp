<?php

namespace Altra\Amqp\Commands;

use Altra\Amqp\Facades\Amqp;
use Altra\Amqp\FailedConsumer;
use Illuminate\Console\Command;
use PhpAmqpLib\Message\AMQPMessage;

class AmqpConsumeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amqp:consume';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to consume from microservice rabbitmq queue';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Consuming...');
        Amqp::consume(config('amqp.queue'), function (AMQPMessage $message) {
            try {
                $this->info('Processing message: '.$message->getDeliveryTag());
                $action = app(config('amqp.consume_actions.'.$message->getRoutingKey()));
                $action->execute($message);
                $message->getChannel()->basic_ack($message->getDeliveryTag());
                $this->info('Message processed!');
            } catch (\Throwable$th) {
                app('amqp_consumer')->reject($message);
                FailedConsumer::create([
                    'message'    => $th->getMessage(),
                    'connection' => $message->getChannel()->getChannelId(),
                    'queue'      => config('amqp.queue'),
                    'payload'    => $message->body,
                    'exception'  => $th->getTraceAsString(),
                ]);
                $this->error($message->getRoutingKey().$th->getMessage());
            }
        });
    }
}
