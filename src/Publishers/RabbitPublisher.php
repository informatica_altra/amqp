<?php

namespace Altra\Amqp\Publishers;

use Altra\Amqp\Connections\RabbitMQConnection;
use Altra\Amqp\Contracts\PublisherContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitPublisher extends RabbitMQConnection implements PublisherContract
{
    /**
     * @var bool
     */
    public $publishResult = true;

    /**
     * @var AMQPMessage
     */
    public $msg;

    public function publish(mixed $message, array $exchange, string $routing, array $queues = null, bool $mandatory = false): bool
    {
        $msg = new AMQPMessage($message);

        // Declare exchange
        $this->getChannel()->exchange_declare($exchange[
      'exchange'],
      $exchange['type'],
      false,
      true,
      false
    );

        // Declare queue
        if ($queues != null) {
            foreach ($queues as $queue) {
                $this->getChannel()->queue_declare($queue, false, true, false, false);
                // Bind exchange to queue
                $this->getChannel()->queue_bind($queue, $exchange['exchange'], $routing);
            }
        }

        // Publish message
        $this->getChannel()->basic_publish($msg, $exchange['exchange'], $routing, $mandatory);

        // Close connection
        $this->shutdown($this->getChannel(), $this->getConnection());

        return $this->publishResult;
    }

    public function publishBatch(Collection $collection, array $exchange, string $routing, array $queues = null, bool $mandatory = false): bool
    {
        $collection->map(function ($item) use ($exchange, $routing, $queues, $mandatory) {
            try {
                /** @var Model $item */
                $msg = new AMQPMessage($item->toJson());

                // Declare exchange
                $this->getChannel()->exchange_declare($exchange[
          'exchange'],
          $exchange['type'],
          false,
          true,
          false
        );

                // Declare queue
                if ($queues != null) {
                    foreach ($queues as $queue) {
                        $this->getChannel()->queue_declare($queue, false, true, false, false);
                        // Bind exchange to queue
                        $this->getChannel()->queue_bind($queue, $exchange['exchange'], $routing);
                    }
                }

                // Publish message
                $this->getChannel()->basic_publish($msg, $exchange['exchange'], $routing, $mandatory);
            } catch (\Throwable$th) {
                Log::critical($th->getMessage(), $item->toArray());
            }
        });

        // Close connection
        $this->shutdown($this->getChannel(), $this->getConnection());

        return $this->publishResult;
    }
}
