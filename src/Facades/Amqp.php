<?php

namespace Altra\Amqp\Facades;

use Altra\Amqp\Testing\AmqpFake;
use Illuminate\Support\Facades\Facade;

/**
 * @method static bool publish (mixed $message, array $exchange, string $routing, array $queues = null)
 * @method static bool publishBatch(Collection $collection, array $exchange, string $routing, array $queues = null)
 * @method static void consume(string $queue, Closure $callback, array $properties = [])
 */
class Amqp extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'amqp';
    }

    public static function fake()
    {
        static::swap($fake = new AmqpFake);

        return $fake;
    }
}
