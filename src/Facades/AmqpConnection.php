<?php

namespace Altra\Amqp\Facades;

use Illuminate\Support\Facades\Facade;

class AmqpConnection extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'amqp_connection';
    }
}
